------------------------------------------------------------------------------
--  File: TKT_tb.vhd
------------------------------------------------------------------------------
-- TKT testbench
library IEEE;
use IEEE.std_logic_1164.all;

entity TKT_tb is
end TKT_tb;

architecture testbench of TKT_tb is

--component Structural_TKT is
	--port(
		
	--);
--end component Structural_TKT;

component Behavioral_TKT is
	port(
		--x0, x1, x2, x3, x4, x5 : in std_logic_vector(6 downto 0);
		x : in std_logic_vector(5 downto 0);
		--y0, y1, y2 : out std_logic;
		y : out std_logic_vector(2 downto 0);
	);
end component Behavioral_TKT;

--component Dataflow_TKT is
	--port(
		--x0, x1, x2, x3, x4, x5 : in std_logic;
		--y0, y1, y2 : out std_logic;
	--);
--end component Dataflow_TKT;

--signal x0_tb, x1_tb, x2_tb, x3_tb, x4_tb, x5_tb, y0_tb, y1_tb, y2_tb : in std_logic_vector(6 downto 0);
signal x_tb : std_logic_vector(5 downto 0);
signal y_tb : std_logic_vector(2 downto 0);
--signal D0_tb, D1_tb, D2_tb, D3_tb, struct_out, beh_out, data_out : std_logic_vector (4 downto 0);
--signal a_tb, b_tb : std_logic;

begin
	behav_m : Behavioral_TKT
		port map(
			x => x_tb,
			y => y_tb
		);
		
	--struct_m : Structural_TKT
		--port map(
			--D0 => D0_tb,
			--D1 => D1_tb,
			--D2 => D2_tb,
			--D3 => D3_tb,
			--a => a_tb,
			--b => b_tb,
			--out_3 => struct_out
		--);
	
	--data_m : Dataflow_TKT
		--port map(
			--D0 => D0_tb,
			--D1 => D1_tb,
			--D2 => D2_tb,
			--D3 => D3_tb,
			--a => a_tb,
			--b => b_tb,
			--out_3 => data_out
		--);
	process
	begin
		x_tb <= "000010";
		-- Test 1
		--D0_tb <= "00000";
		--D1_tb <= "11111";
		--D2_tb <= "11100";
		--D3_tb <= "00111";
		
		--a_tb <= '0';
		--b_tb <= '0';
		--wait for 25 ns;
 
		--a_tb <= '0';
		--b_tb <= '1';
		--wait for 25 ns;
 
		--a_tb <= '1';
		--b_tb <= '0';
		--wait for 25 ns;
 
		--a_tb <= '1';
		--b_tb <= '1';
		wait for 25 ns;
		
		--wait; --suspend
	end process;
end testbench;