library IEEE;
use IEEE.std_logic_1164.all;

entity Dataflow_TKT is
	port (
		x0, x1, x2, x3, x4, x5 : in std_logic_vector(6 downto 0);
		y0, y1, y2 : out std_logic_vector(3 downto 0);
	);
end Dataflow_TKT;

architecture dataflow of Dataflow_TKT is
	
	--signal a5, b5 : std_logic_vector(4 downto 0);
	begin
	y0 <= (
				(x2 and x3 and x4 and (not x5)) or
				(x2 and (not x3) and x4 and x5) or
				((not x0))
			);
			
end dataflow;