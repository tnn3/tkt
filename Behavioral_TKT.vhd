library IEEE;
use IEEE.std_logic_1164.all;
 
entity Behavioral_TKT is
	port (
		--x0, x1, x2, x3, x4, x5 : in std_logic_vector(6 downto 0);
		x : in std_logic_vector(5 downto 0);
		--y0, y1, y2 : out std_logic;
		y : out std_logic_vector(2 downto 0);
	);
end Behavioral_TKT;

architecture behavioral of Behavioral_TKT is
begin
  --process ( x0, x1, x2, x3, x4, x5 ) begin
  	process ( x ) begin
  	if x = "000010" then
  		y <= "1";
	end if;
  end process;
end behavioral;